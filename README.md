# BitmapCanary大图检测工具
![](./mdres/bitmapcanary-logo.png)
## 这是什么
开发过程中，因为项目管理等原因，往往会忽视应用内存性能问题，如一个30×30的View控件却加载了800×800的图片，这样场景下的内存利用率是极低的。使用该工具可以及时发现此类内存性能问题，无侵入，无反射，无兼容性问题(大概)。
## 如何使用
使用很简单，项目中添加以下依赖即可
```
debugImplementation(name: 'BitmapCanary-debug', ext: 'aar')
```
## 查看纪录
### 吐司提示
当加载大图所在页面进行跳转或销毁时，可触发吐司

![](./mdres/img_screenshot.png)

### logcat提示
logcat中搜索关键字 **BitmapCanary** 即可

![](./mdres/img_logcat.png)
### 日志文件
日志文件记录在：/data/user/0/[packageName]/files/BitmapCanary_yyyyMMdd.log
例如：
```
/data/user/0/com.pengyeah.bitmapcanarydemo/files/BitmapCanary_20210427.log
```
## 定制
默认配置为当View控件加载的图片宽高大于自身的 1.2 倍时触发警告，可修改如下配置
### 警告倍率
默认 1.2F
```kotlin
BitmapCanary.rate = 2F
```
### 是否输出logcat
默认输出
```kotlin
BitmapCanary.isOpenLog = false
```
### 是否输出文件
默认输出
```kotlin
BitmapCanary.isWriteLog = false
```
## 架构设计

![](./mdres/jiagou.png)
## 版本更迭

|  版本号   | 修订人  | 内容 | 备注 |
|  ----  | ----  | ---- | ---- |
| 1.0.0  | pengyeah | 初版 | 无 |

## FAQ