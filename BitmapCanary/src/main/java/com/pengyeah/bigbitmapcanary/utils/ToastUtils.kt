package com.pengyeah.bigbitmapcanary.utils

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import com.pengyeah.bigbitmapcanary.R

internal fun showToast(context: Context, content: String) {
    val appContext = context.applicationContext
    val view = LayoutInflater.from(appContext).inflate(R.layout.layout_toast, null)
    val tv = view.findViewById<TextView>(R.id.tv)
    tv.text = content
    val toast = Toast.makeText(appContext, content, Toast.LENGTH_LONG)
    toast.view = view
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}