package com.pengyeah.bigbitmapcanary.utils

import java.text.SimpleDateFormat
import java.util.*

internal fun formatDate(date: Date): String {
    return SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date)
}