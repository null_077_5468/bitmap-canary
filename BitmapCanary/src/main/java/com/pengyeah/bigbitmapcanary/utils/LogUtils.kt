package com.pengyeah.bigbitmapcanary.utils

import android.util.Log

internal object LogUtils {
    var isOpenLog: Boolean = true

    fun i(tag: String, content: String) {
        if (isOpenLog) {
            Log.i(tag, content)
        }
    }
}