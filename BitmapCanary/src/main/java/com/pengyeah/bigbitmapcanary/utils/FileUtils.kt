package com.pengyeah.bigbitmapcanary.utils

import okio.buffer
import okio.sink
import java.io.File
import java.nio.charset.Charset


/**
 * 写文件
 */
internal fun writeLog(filePath: String, content: String) {
    val file = File(filePath)
    file.sink().buffer().writeString(content, Charset.forName("utf-8")).close()
}