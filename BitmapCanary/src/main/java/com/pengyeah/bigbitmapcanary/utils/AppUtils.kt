package com.pengyeah.bigbitmapcanary.utils

import android.content.Context
import android.view.View
import com.pengyeah.bigbitmapcanary.BitmapCanary
import java.lang.Exception

/**
 * 根据view id反查view id name
 */
internal fun findViewIDNameByView(context: Context, view: View): String? {
    if (view.id == View.NO_ID) {
        return null
    }
    var viewName: String? = null
    try {
        viewName = context.resources.getResourceEntryName(view.id)
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return viewName
}

/**
 * 根据view获取activity name
 */
internal fun findActivityNameByView(view: View): String {
    return view.context::class.java.name
}