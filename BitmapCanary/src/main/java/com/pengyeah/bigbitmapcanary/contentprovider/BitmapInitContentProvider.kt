package com.pengyeah.bigbitmapcanary.contentprovider

import android.app.Application
import android.content.ContentProvider
import android.content.ContentValues
import android.database.Cursor
import android.net.Uri
import android.util.Log
import com.pengyeah.bigbitmapcanary.BitmapCanary
import com.pengyeah.bigbitmapcanary.constant.Constant

/**
 * @author pengyeah
 * 用于初始化 BitmapCanary
 */
internal class BitmapInitContentProvider : ContentProvider() {

    override fun onCreate(): Boolean {
        Log.i(Constant.TAG, "BitmapInitContentProvider onCreate")
        this.context?.let {
            BitmapCanary.install(it.applicationContext as Application)
        }
        return true
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        return null
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return null
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        return 0
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        return 0
    }

}