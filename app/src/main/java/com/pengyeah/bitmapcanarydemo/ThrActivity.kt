package com.pengyeah.bitmapcanarydemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class ThrActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thr)
        supportFragmentManager.beginTransaction().replace(R.id.layoutContent, ThrFragment()).commit()
    }

    class ThrFragment : Fragment() {
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val contentView = inflater.inflate(R.layout.fragment_thr, container, false)
            return contentView
        }
    }
}